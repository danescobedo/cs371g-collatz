# CS371g: Generic Programming Collatz Repo

* Name: Daniel Escobedo

* EID: dje696

* GitLab ID: danescobedo

* HackerRank ID: dan_escobedo714

* Git SHA: a1fd6234aa23c2818efdfffe23e3d704e769dcf6

* GitLab Pipelines: https://gitlab.com/danescobedo/cs371g-collatz

* Estimated completion time: 15 hours

* Actual completion time: N/A

* Comments: Borrowed code from previous OOP semester
