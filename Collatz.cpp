// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (istream_iterator<int>& begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);}
    
    
    
    
int calcCycle(int x){
    int count = 0;
    while(x != 1){
        if(x%2 == 1){
            x = x*3 + 1;
            x /= 2;
            ++++count;
        }
        else{
            x /= 2;
            ++count;
        }
    }
    //For when x == 1
    ++count;
    return count;
}


// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    tie(i, j) = p;
    // <your code>
    
    int maxLen = 0;
    for(; i <= j; ++i){
        int currLen = calcCycle(i);
        if(currLen > maxLen)
            maxLen = currLen;
    }

    return make_tuple(i, j, maxLen);}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    while (begin_iterator != end_iterator) {
         collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));}}
